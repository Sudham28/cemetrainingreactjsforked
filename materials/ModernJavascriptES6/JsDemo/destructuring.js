// let [a, b, c] = [1, 2, 3];
// a = a + 1;
// b = b - 1;
// console.log(`a is ${a}`);
// console.log(`b is ${b}`);

// function displayPoint({ x = 0, y = 0, z = 0 } = { x: 1, y: 2, z: 3 }) {
//   console.log(x, y, z);
// }

// displayPoint({ x: 10, z: 30 }); // 10 0 30
// displayPoint({}); // 0 0 0
// displayPoint(); // 1 2 3

function convertEuros(euroAmount) {
  return {
    USD: euroAmount * 1.17,
    GBP: euroAmount * 0.89,
    NOK: euroAmount * 9.27,
  };
}

let convertAmt = convertEuros(100);
console.log(`US Dollars is ${convertAmt.USD}`);

let { USD, GBP, NOK } = convertEuros(100);
console.log(`GBP: ${GBP}`);
