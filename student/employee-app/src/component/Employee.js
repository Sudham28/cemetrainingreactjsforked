import React, { useEffect, useState } from "react";
import EmployeeName from "./EmployeeName";
import EmployeeDetails from "./EmployeeDetails";
import ShowHideButton from "./ShowhideButton";

const Employee = (props) => {
  const [visible, setVisible] = useState(true);
  return (
    <div>
      <div className="col-md-4">
        <div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
          <div className="card-body">
            <EmployeeName name={props.employee.name} />
            {visible && (
              <EmployeeDetails
                age={props.employee.age}
                salary={props.employee.salary}
                email={props.employee.email}
              />
            )}
            <ShowHideButton toggle={setVisible} visible={visible} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Employee;
