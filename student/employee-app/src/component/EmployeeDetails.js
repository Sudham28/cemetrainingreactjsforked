const EmployeeDetails = (props) => {
  return (
    <div>
      <ol>
        <li>{props.age}</li>
        <li>{props.salary}</li>
        <li>{props.email}</li>
      </ol>
    </div>
  );
};

export default EmployeeDetails;
