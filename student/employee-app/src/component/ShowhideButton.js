import React from "react";

const ShowHideButton = (props) => (
  <button
    className="btn btn-sm btn-outline-secondary"
    onClick={() => props.toggle(!props.visible)}
  >
    {props.visible ? "Hide" : "Show"}
  </button>
);

export default ShowHideButton;
