import Employee from "./Employee";
import Banner from "./Banner";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import CreateEmployeeForm from "./CreateEmployeeForm";

const App = () => {
  const [employees, setEmployees] = useState([]);
  const [fetchEmployee, setFetchEmployee] = useState(false);
  useEffect(() => {
    axios.get("http://localhost:8080/employee/all").then((res) => {
      console.log(res);
      setEmployees(res.data);
    });
  }, [fetchEmployee]);
  return (
    <Router>
      <div className="App">
        <ul className="nav">
          <li className="nav-item">
            <NavLink
              className="nav-link text-secondary"
              to="/"
              activeClassName="app"
            >
              Home
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              exact
              className="nav-link text-secondary"
              to="/add"
              activeClassName="app"
            >
              Add Album
            </NavLink>
          </li>
        </ul>
        <Banner />
        <switch>
          <Route exact path="/">
            <div className="container">
              <div className="row">
                {employees.map((emp) => (
                  <Employee key={emp.id} employee={emp} />
                ))}
              </div>
            </div>
          </Route>
          <Route exact path="/add">
            <div className="container">
              <div className="row">
                <CreateEmployeeForm
                  fetchEmployee={fetchEmployee}
                  setFetchEmployee={setFetchEmployee}
                />
              </div>
            </div>
          </Route>
        </switch>
      </div>
    </Router>
  );
};

export default App;
