import { render } from "react-dom";

const EmployeeName = (props) => {
  return <h5 className="album-title card-title">{props.name}</h5>;
};

export default EmployeeName;
