import React from "react";

const AlbumTitle = (props) => (
	<h5 className="album-title card-title">{props.title}</h5>
);

export default AlbumTitle;
