import React from "react";

import Greet from "./Greet";

class Person extends React.Component {
	constructor(props) {
		super(props);

		// set the inital state
		this.state = { height: 185, weight: 200 };

		//this.grow = this.grow.bind(this);
	}

	// grow() {
	// 	this.setState({ height: this.state.height + 1 });
	// }

	componentDidMount() {
		// call api and setState with api data
		console.log("it rendered!");
	}

	componentDidUpdate() {
		console.log("it rendered!");
	}

	growHeight = () => {
		//this.state.height = this.state.height + 1;
		//console.log(this.state);
		this.setState((state) => {
			return {
				height: state.height + 1,
			};
		}); // height: 185 + 1
	};

	growWeight = () => {
		//this.state.height = this.state.height + 1;
		//console.log(this.state);
		this.setState((state) => {
			return {
				weight: state.weight + 1,
			};
		});
	};

	render() {
		return (
			<div>
				<Greet person={this.props.name} name="AllState" />
				<p>
					{this.props.name} weighs {this.state.weight}lbs and is{" "}
					{this.state.height}
					cm tall.
				</p>
				<button onClick={this.growWeight}>Grow weight</button>
				<button onClick={this.growHeight}>Grow height</button>
			</div>
		);
	}
}

export default Person;
